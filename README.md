1. docker-compose up -d
2. создать схему из консоли
    
    CREATE KEYSPACE mdm WITH replication = {'class': 'SimpleStrategy', 'replication_factor' : 1};
      
3. запустить приложение
4. если в application.yml app.data.initialize=true предзаполнятся 2 пользователя + 1 администратор 
5. запросы:
    
    5.1 аутентификация
    
        mutation {
          login(authenticationInput: {
            email: "admin@gmail.com"
            password: "admin"
          }) {
            token, email
          }
        }
    
    (!) далее ко всем запросам добавляем заголовок
        
        {
            "Authorization": "Bearer ${token}"
        }     
    
    5.2 получить всех пользователей
        
        query {
           getAllUsers {
             id, name
          }
        }
    5.3 получить пользователя по идентификатору
        
        query{
          getUserById(
            id: "4ce322c0-ad47-11ec-b14f-4f84183f46f8"
          ){
            id, email, name, surname, roles, avatar
          }
        }
    5.4 удалить пользователя
        
        mutation{
          removeUser(
            id: "4fdc8ca0-ad47-11ec-b14f-4f84183f46f8"
          )
        }

    5.5 обновить пользователя
        
        mutation{
          updateUser(
            updateUserInput:{
              avatar:"base64string",
              email: "admin42@gmail.com",
              id: "74dcb710-ad55-11ec-928d-973236904b1b",
              name: "admin_name",
              password:"123",
              roles:[ROLE_USER, ROLE_ADMIN],
              surname: "admin_surname"
            }){
              id
            }
        }
     5.6 создать пользователя
     
        mutation{
          createUser(
            createUserInput:{
              avatar:"base64string",
              email: "mail@mail.ru",
              name:"user_name",
              password:"user_password",
              roles:[ROLE_USER],
              surname: "user_surname"
            }){
              id, email, name, surname
            }
        }     
        