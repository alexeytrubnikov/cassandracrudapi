package ru.app.casandra.controller;

import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLNonNull;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import ru.app.casandra.domain.user.User;
import ru.app.casandra.dto.AuthRequest;
import ru.app.casandra.dto.AuthResponse;
import ru.app.casandra.dto.UserDto;
import ru.app.casandra.service.authentication.AuthenticationService;
import ru.app.casandra.service.user.UserService;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Component
@GraphQLApi
public class UserGraphqlController {
    private final UserService userService;
    private final AuthenticationService authenticationService;

    @GraphQLMutation(name = "login")
    public AuthResponse login(@GraphQLArgument(name = "authenticationInput") @GraphQLNonNull AuthRequest request) {

        return AuthResponse.builder()
                .email(request.getEmail())
                .token(authenticationService.authenticateAndReturnToken(request))
                .build();

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GraphQLQuery(name = "getAllUsers")
    public List<UserDto> findAllUsers() {

        return userService.findAll();

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GraphQLQuery(name = "getUserById")
    public UserDto findUserById(@GraphQLArgument(name = "id") UUID id) {

        return userService.findById(id);

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GraphQLMutation(name = "createUser", description = "create new user")
    public UserDto createUser(@GraphQLArgument(name = "createUserInput") @GraphQLNonNull User user) {
        return userService.createUser(user);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GraphQLMutation(name = "updateUser", description = "update user")
    public UserDto updateUser(@GraphQLArgument(name = "updateUserInput") @GraphQLNonNull User user) {
        return userService.updateUser(user);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GraphQLMutation(name = "removeUser", description = "remove user by id")
    public void removeUser(@GraphQLArgument(name = "id") UUID id) {
        userService.removeById(id);
    }


}
