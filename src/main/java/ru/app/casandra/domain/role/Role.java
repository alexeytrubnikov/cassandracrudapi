package ru.app.casandra.domain.role;

public enum Role {
    ROLE_ADMIN(){
        @Override
        public String getRoleName() {
            return "ADMIN";
        }
    },
    ROLE_USER(){
        @Override
        public String getRoleName() {
            return "USER";
        }
    };

    public abstract String getRoleName();


}
