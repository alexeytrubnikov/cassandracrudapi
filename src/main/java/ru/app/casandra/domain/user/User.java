package ru.app.casandra.domain.user;

import io.leangen.graphql.annotations.GraphQLId;
import io.leangen.graphql.annotations.GraphQLInputField;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;
import ru.app.casandra.domain.role.Role;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Table(value = "users")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(of = {"id"})
public class User implements Serializable {

    @PrimaryKey
    @GraphQLId
    private UUID id;

    @NotNull
    @NotBlank
    @NotEmpty
    @GraphQLInputField
    private String name;

    @NotNull
    @NotBlank
    @NotEmpty
    @GraphQLInputField
    private String surname;

    @NotNull
    @NotBlank
    @NotEmpty
    @Email
    @GraphQLInputField
    private String email;

    @NotNull
    @GraphQLInputField
    private List<Role> roles;

    @GraphQLInputField
    private String avatar;

    @GraphQLInputField
    @NotNull
    @NotBlank
    private String password;

}
