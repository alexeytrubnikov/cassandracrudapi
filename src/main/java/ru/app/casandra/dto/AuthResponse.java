package ru.app.casandra.dto;

import io.leangen.graphql.annotations.GraphQLInputField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class AuthResponse {
    @GraphQLInputField
    private String email;
    @GraphQLInputField
    private String token;
}
