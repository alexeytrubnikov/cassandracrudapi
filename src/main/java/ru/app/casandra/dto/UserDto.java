package ru.app.casandra.dto;

import io.leangen.graphql.annotations.GraphQLId;
import io.leangen.graphql.annotations.GraphQLInputField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.app.casandra.domain.role.Role;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class UserDto {
    @GraphQLId
    private UUID id;
    @GraphQLInputField
    private String name;
    @GraphQLInputField
    private String surname;
    @GraphQLInputField
    private String email;
    @GraphQLInputField
    private List<Role> roles;
    @GraphQLInputField
    private String avatar;
}
