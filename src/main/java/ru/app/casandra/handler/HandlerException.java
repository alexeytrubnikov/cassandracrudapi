package ru.app.casandra.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.app.casandra.exception.DuplicateUserException;
import ru.app.casandra.exception.UserNotFoundException;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class HandlerException {

    private ResponseEntity<Object> getResponseEntity(RuntimeException ex, HttpStatus status) {

        Map<String, String> response = new HashMap<>();
        response.put("message", ex.getMessage());
        return new ResponseEntity<>(response, status);

    }

    @ExceptionHandler(value = UserNotFoundException.class)
    protected ResponseEntity<Object> userNotFoundException(UserNotFoundException ex) {
        return getResponseEntity(ex, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = DuplicateUserException.class)
    protected ResponseEntity<Object> duplicateUserException(DuplicateUserException ex) {
        return getResponseEntity(ex, HttpStatus.BAD_REQUEST);
    }


}

