package ru.app.casandra.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import ru.app.casandra.domain.user.User;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends CassandraRepository<User, UUID> {
    @Query(allowFiltering = true)
    Optional<User> findFirstByEmail(String email);
}
