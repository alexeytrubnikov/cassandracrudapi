package ru.app.casandra.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.app.casandra.domain.user.User;
import ru.app.casandra.exception.UserNotFoundException;
import ru.app.casandra.service.mapper.UserMapper;
import ru.app.casandra.service.user.UserService;

@Service
@Slf4j
@RequiredArgsConstructor
public class JwtUserDetailsService implements UserDetailsService {

    private final UserService userService;
    private final UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user;

        try{
            user = userService.findByEmail(username);
        }catch (UserNotFoundException ex) {
            log.error(ex.getMessage());
            throw new UsernameNotFoundException(String.format("User with email: %s, not found", username));
        }

        var jwtUser = userMapper.toJwt(user);
        log.info("loadUserByUsername - user with email: {} successfully loaded", username);

        return jwtUser;

    }
}
