package ru.app.casandra.service.authentication;

import ru.app.casandra.dto.AuthRequest;

public interface AuthenticationService {
    String authenticateAndReturnToken(AuthRequest request);
}
