package ru.app.casandra.service.authentication.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.app.casandra.dto.AuthRequest;
import ru.app.casandra.exception.UserNotFoundException;
import ru.app.casandra.security.jwt.JwtTokenProvider;
import ru.app.casandra.service.authentication.AuthenticationService;
import ru.app.casandra.service.user.UserService;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthenticationServiceImpl implements AuthenticationService {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final UserService userService;

    @Override
    public String authenticateAndReturnToken(AuthRequest request) {

        try {

            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getEmail(),
                    request.getPassword()));
            var user = userService.findByEmail(request.getEmail());

            return jwtTokenProvider.createToken(user.getEmail());

        }catch (UserNotFoundException ex) {

            throw new UsernameNotFoundException(String.format("User with email: %s, not found", request.getEmail()));

        }catch (AuthenticationException ex) {

            throw new BadCredentialsException("Invalid email or password");

        }

    }
}
