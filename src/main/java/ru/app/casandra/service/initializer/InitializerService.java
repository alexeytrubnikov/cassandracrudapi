package ru.app.casandra.service.initializer;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import ru.app.casandra.domain.role.Role;
import ru.app.casandra.domain.user.User;
import ru.app.casandra.service.user.UserService;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
@RequiredArgsConstructor
@ConditionalOnProperty(value = "app.data.initialize", havingValue = "true")
public class InitializerService {

    private final UserService userService;

    @PostConstruct
    public void init() {

        List<User> users = List.of(User.builder()
                        .name("admin")
                        .surname("admin")
                        .email("admin@gmail.com")
                        .avatar("adminAvatarBase64")
                        .password("admin")
                        .roles(List.of(Role.ROLE_ADMIN, Role.ROLE_USER))
                        .build(),
                User.builder()
                        .name("user_1")
                        .surname("user_1")
                        .email("user_1@gmail.com")
                        .avatar("userAvatarBase64")
                        .password("user_1")
                        .roles(List.of(Role.ROLE_USER))
                        .build(),
                User.builder()
                        .name("user_2")
                        .surname("user_2")
                        .email("user_2@gmail.com")
                        .avatar("userAvatarBase64")
                        .password("user_2")
                        .roles(List.of(Role.ROLE_USER))
                        .build());

        userService.removeAll();
        users.forEach(userService::createUser);

    }
}
