package ru.app.casandra.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import ru.app.casandra.domain.role.Role;
import ru.app.casandra.domain.user.User;
import ru.app.casandra.dto.UserDto;
import ru.app.casandra.security.jwt.JwtUser;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(source = "roles", target = "authorities")
    JwtUser toJwt(User user);

    default List<GrantedAuthority> userRoleToGrantedAuthority(List<Role> roles) {
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.name()))
                .collect(Collectors.toList());
    }

    UserDto toDto(User user);

}
