package ru.app.casandra.service.user;

import ru.app.casandra.domain.user.User;
import ru.app.casandra.dto.UserDto;

import java.util.List;
import java.util.UUID;

public interface UserService {

    UserDto createUser(User user);

    UserDto findById(UUID id);

    UserDto updateUser(User user);

    void removeById(UUID id);
    void removeAll();

    List<UserDto> findAll();

    User findByEmail(String email);

}
