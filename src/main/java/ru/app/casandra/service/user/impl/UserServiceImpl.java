package ru.app.casandra.service.user.impl;

import com.datastax.oss.driver.api.core.uuid.Uuids;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.app.casandra.domain.role.Role;
import ru.app.casandra.domain.user.User;
import ru.app.casandra.dto.UserDto;
import ru.app.casandra.exception.DuplicateUserException;
import ru.app.casandra.exception.UserNotFoundException;
import ru.app.casandra.repository.UserRepository;
import ru.app.casandra.service.mapper.UserMapper;
import ru.app.casandra.service.user.UserService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final UserMapper userMapper;

    @Override
    public UserDto createUser(User user) {

        if (userRepository.findFirstByEmail(user.getEmail()).isPresent()) {
            throw new DuplicateUserException("User with email already exists");
        }

        user.setId(Optional.ofNullable(user.getId())
                .orElse(Uuids.timeBased()));

        user.setRoles(Optional.ofNullable(user.getRoles())
                .orElse(List.of(Role.ROLE_USER)));

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        var regUser = userRepository.save(user);

        var dto = userMapper.toDto(regUser);

        log.info("createUser - user: {} successfully created", dto);

        return dto;

    }

    @Override
    public UserDto findById(UUID id) {

        var user = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(String.format("User by id %s, not found", id)));

        return userMapper.toDto(user);

    }

    @Override
    public UserDto updateUser(User user) {

        var findUser = userRepository.findById(user.getId())
                .orElseThrow(() -> new UserNotFoundException(String.format("User by id: %s, not found", user.getId())));

        user.setPassword(Optional.ofNullable(user.getPassword())
                .map(passwordEncoder::encode)
                .orElse(findUser.getPassword()));

        user.setRoles(Optional.ofNullable(user.getRoles())
                .orElse(findUser.getRoles()));

        user.setAvatar(Optional.ofNullable(user.getAvatar())
                .orElse(findUser.getAvatar()));

        user.setEmail(Optional.ofNullable(user.getEmail())
                .orElse(findUser.getEmail()));

        user.setName(Optional.ofNullable(user.getName())
                .orElse(findUser.getName()));

        user.setSurname(Optional.ofNullable(user.getSurname())
                .orElse(findUser.getSurname()));

        var saveUser = userRepository.save(user);

        var dto = userMapper.toDto(saveUser);

        log.info("updateUser - user: {} successfully updated", dto);
        return dto;

    }

    @Override
    public void removeById(UUID id) {

        final User findUser = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(String.format("User by id %s, not found", id)));

        log.info("removeById - user with id: {} successfully removed", id);

        userRepository.delete(findUser);
    }

    @Override
    public void removeAll() {
        userRepository.deleteAll();
    }

    @Override
    public List<UserDto> findAll() {

        var findUsers = userRepository.findAll();

        log.info("findAll - {} users found", findUsers.size());

        return findUsers.stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());

    }

    @Override
    public User findByEmail(String email) {

        return userRepository.findFirstByEmail(email)
                .orElseThrow(() -> new UserNotFoundException(String.format("User by email %s, not found", email)));

    }
}
