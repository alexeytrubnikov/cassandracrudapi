package ru.app.casandra.security;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.app.casandra.exception.UserNotFoundException;
import ru.app.casandra.service.mapper.UserMapper;
import ru.app.casandra.service.user.UserService;
import ru.app.casandra.util.TestDataFactory;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class JwtUserDetailsServiceTest {

    @Mock
    UserService userService;
    @Mock
    UserMapper userMapper;

    @InjectMocks
    JwtUserDetailsService userDetailsService;

    @Test
    void loadUserByUsernameSuccessfully() {

        //GIVEN
        var entity = TestDataFactory.getUserEntity();
        var jwt = TestDataFactory.getJwtUser();

        doReturn(entity)
                .when(userService)
                .findByEmail(entity.getEmail());

        doReturn(jwt)
                .when(userMapper)
                .toJwt(entity);

        //WHEN

        var result = userDetailsService.loadUserByUsername(entity.getEmail());

        //THEN
        assertEquals(result, jwt);
        verify(userService, times(1)).findByEmail(entity.getEmail());
        verify(userMapper, times(1)).toJwt(entity);

    }

    @Test
    void loadUserByUsernameFailUsernameNotFound() {

        //GIVEN
        var entity = TestDataFactory.getUserEntity();

        doThrow(UserNotFoundException.class)
                .when(userService)
                .findByEmail(entity.getEmail());

        //WHEN

        var ex = assertThrows(UsernameNotFoundException.class, () -> userDetailsService.loadUserByUsername(entity.getEmail()));

        //THEN
        assertTrue(ex.getMessage().contains("User with email:"));
        verify(userService, times(1)).findByEmail(entity.getEmail());
        verify(userMapper, times(0)).toJwt(entity);

    }

}