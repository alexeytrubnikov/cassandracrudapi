package ru.app.casandra.service.user;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.app.casandra.exception.UserNotFoundException;
import ru.app.casandra.repository.UserRepository;
import ru.app.casandra.service.mapper.UserMapper;
import ru.app.casandra.service.user.impl.UserServiceImpl;
import ru.app.casandra.util.TestDataFactory;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    UserRepository userRepository;
    @Mock
    BCryptPasswordEncoder passwordEncoder;
    @Mock
    UserMapper userMapper;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    void createUser() {

        //GIVEN
        var user = TestDataFactory.getNewUserEntity();
        var userDto = TestDataFactory.getUserDto();

        doReturn(Optional.empty()).when(userRepository)
                .findFirstByEmail(anyString());

        doReturn(user.getPassword())
                .when(passwordEncoder)
                .encode(anyString());

        doReturn(user)
                .when(userRepository)
                .save(user);

        doReturn(userDto)
                .when(userMapper)
                .toDto(user);

        //WHEN
        var result = userService.createUser(user);

        //THEN
        assertTrue(result.getId() != null);

        verify(userRepository, times(1))
                .findFirstByEmail(anyString());

        verify(userMapper, times(1))
                .toDto(user);

        verify(passwordEncoder, times(1))
                .encode(anyString());

        verify(userRepository, times(1)).save(user);



    }

    @Test
    void findByIdSuccessfully() {

        //GIVEN
        var entity = TestDataFactory.getAdminEntity();
        var dto = TestDataFactory.getAdminDto();


        doReturn(Optional.of(entity)).when(userRepository)
                .findById(entity.getId());

        doReturn(dto)
                .when(userMapper)
                .toDto(entity);

        //WHEN
        var result = userService.findById(entity.getId());

        //THEN
        assertEquals(result, dto);
        verify(userRepository, times(1)).findById(entity.getId());
        verify(userMapper, times(1)).toDto(entity);

    }

    @Test
    void findByIdFail() {

        //GIVEN
        var entity = TestDataFactory.getAdminEntity();

        doReturn(Optional.empty()).when(userRepository)
                .findById(entity.getId());
        //WHEN
        var ex = assertThrows(UserNotFoundException.class, () -> userService.findById(entity.getId()));

        //THEN
        assertTrue(ex.getMessage().contains("User by id"));
        verify(userRepository, times(1)).findById(entity.getId());
        verify(userMapper, times(0)).toDto(entity);

    }

    @Test
    void updateUserSuccessfully() {

        //GIVEN

        var updateEntity= TestDataFactory.getUserEntity();
        var findEntity = TestDataFactory.getUserEntity();
        var saveEntity = TestDataFactory.getUserEntity();
        var dto = TestDataFactory.getUserDto();

        doReturn(Optional.of(findEntity))
                .when(userRepository)
                .findById(updateEntity.getId());

        doReturn(updateEntity.getPassword())
                .when(passwordEncoder)
                .encode(updateEntity.getPassword());

        doReturn(saveEntity)
                .when(userRepository)
                .save(updateEntity);

        doReturn(dto)
                .when(userMapper)
                .toDto(saveEntity);

        //WHEN

        var result = userService.updateUser(updateEntity);

        //THEN

        assertEquals(result, dto);
        verify(userRepository, times(1)).findById(updateEntity.getId());
        verify(passwordEncoder, times(1)).encode(updateEntity.getPassword());
        verify(userRepository, times(1)).save(updateEntity);
        verify(userMapper, times(1)).toDto(saveEntity);

    }

    @Test
    void updateUserFailUserNotFound() {

        //GIVEN

        var updateEntity= TestDataFactory.getUserEntity();
        var saveEntity = TestDataFactory.getUserEntity();

        doReturn(Optional.empty())
                .when(userRepository)
                .findById(updateEntity.getId());

        //WHEN
        var ex = assertThrows(UserNotFoundException.class, () -> userService.updateUser(updateEntity));

        //THEN

        assertTrue(ex.getMessage().contains("User by id"));
        verify(userRepository, times(1)).findById(updateEntity.getId());
        verify(passwordEncoder, times(0)).encode(updateEntity.getPassword());
        verify(userRepository, times(0)).save(updateEntity);
        verify(userMapper, times(0)).toDto(saveEntity);

    }

    @Test
    void updateUserSuccessfullyPasswordEncoderNotInvoked() {

        //GIVEN

        var updateEntity= TestDataFactory.getUserEntity();
        updateEntity.setPassword(null);

        var findEntity = TestDataFactory.getUserEntity();
        var saveEntity = TestDataFactory.getUserEntity();
        var dto = TestDataFactory.getUserDto();

        doReturn(Optional.of(findEntity))
                .when(userRepository)
                .findById(updateEntity.getId());

        doReturn(saveEntity)
                .when(userRepository)
                .save(updateEntity);

        doReturn(dto)
                .when(userMapper)
                .toDto(saveEntity);

        //WHEN

        var result = userService.updateUser(updateEntity);

        //THEN

        assertEquals(result, dto);
        assertTrue(updateEntity.getPassword().equals(findEntity.getPassword()));

        verify(userRepository, times(1)).findById(updateEntity.getId());
        verify(passwordEncoder, times(0)).encode(updateEntity.getPassword());
        verify(userRepository, times(1)).save(updateEntity);
        verify(userMapper, times(1)).toDto(saveEntity);

    }

    @Test
    void removeByIdSuccessfully() {

        //GIVEN

        var findEntity = TestDataFactory.getUserEntity();
        var id          = findEntity.getId();

        doReturn(Optional.of(findEntity))
                .when(userRepository)
                .findById(id);

        doNothing()
                .when(userRepository)
                .delete(findEntity);

        //WHEN

        userService.removeById(id);

        //THEN

        verify(userRepository, times(1)).findById(id);
        verify(userRepository, times(1)).delete(findEntity);

    }

    @Test
    void removeByIdFailUserNotFound() {

        //GIVEN

        var findEntity = TestDataFactory.getUserEntity();
        var id          = findEntity.getId();

        doReturn(Optional.empty())
                .when(userRepository)
                .findById(id);

        //WHEN
        var ex = assertThrows(UserNotFoundException.class, () -> userService.removeById(id));

        //THEN
        assertTrue(ex.getMessage().contains("User by id"));
        verify(userRepository, times(1)).findById(id);
        verify(userRepository, times(0)).delete(any());

    }

    @Test
    void removeAll() {

        //GIVEN
        doNothing()
                .when(userRepository)
                .deleteAll();
        //WHEN
        userService.removeAll();

        //THEN
        verify(userRepository, times(1)).deleteAll();


    }

    @Test
    void findAll() {

        //GIVEN
        var findEntities = Collections.singletonList(TestDataFactory.getUserEntity());
        var dto = TestDataFactory.getUserDto();
        doReturn(findEntities)
                .when(userRepository)
                .findAll();

        doReturn(dto)
                .when(userMapper)
                .toDto(any());

        //WHEN

        var result = userService.findAll();

        //THEN

        verify(userRepository, times(1)).findAll();
        verify(userMapper, times(findEntities.size())).toDto(any());

    }

    @Test
    void findByEmailSuccessfully() {

        //GIVEN
        var entity = TestDataFactory.getUserEntity();
        var email = entity.getEmail();

        doReturn(Optional.of(entity))
                .when(userRepository)
                .findFirstByEmail(anyString());

        //WHEN

        var result = userService.findByEmail(email);

        //THEN
        assertEquals(result, entity);
        verify(userRepository, times(1)).findFirstByEmail(anyString());

    }

    @Test
    void findByEmailFailUserNotFound() {

        //GIVEN
        var entity = TestDataFactory.getUserEntity();
        var email = entity.getEmail();

        doReturn(Optional.empty())
                .when(userRepository)
                .findFirstByEmail(anyString());

        //WHEN

        var ex = assertThrows(UserNotFoundException.class, () -> userService.findByEmail(email));

        //THEN
        assertTrue(ex.getMessage().contains("User by email"));
        verify(userRepository, times(1)).findFirstByEmail(anyString());

    }

}