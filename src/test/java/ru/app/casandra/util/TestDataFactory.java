package ru.app.casandra.util;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import ru.app.casandra.domain.role.Role;
import ru.app.casandra.domain.user.User;
import ru.app.casandra.dto.UserDto;
import ru.app.casandra.security.jwt.JwtUser;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class TestDataFactory {

    static UUID userRandomUUID = UUID.randomUUID();
    static UUID adminRandomUUID = UUID.randomUUID();

    public static User getNewUserEntity() {
        return User.builder()
                .password("password")
                .email("mail@mail.ru")
                .surname("user_surname")
                .name("user_name")
                .avatar("base64string")
                .roles(List.of(Role.ROLE_USER))
                .build();
    }

    public static User getUserEntity() {
        return User.builder()
                .id(adminRandomUUID)
                .password("password")
                .email("mail@mail.ru")
                .surname("admin_surname")
                .name("admin_name")
                .avatar("base64string")
                .roles(List.of(Role.ROLE_USER))
                .build();
    }

    public static User getAdminEntity() {
        return User.builder()
                .id(adminRandomUUID)
                .password("password")
                .email("mail@mail.ru")
                .surname("admin_surname")
                .name("admin_name")
                .avatar("base64string")
                .roles(List.of(Role.ROLE_USER, Role.ROLE_ADMIN))
                .build();
    }
    public static UserDto getUserDto() {
        return UserDto.builder()
                .id(userRandomUUID)
                .name("user_name")
                .surname("user_surname")
                .email("mail@mail.ru")
                .roles(List.of(Role.ROLE_USER))
                .avatar("base64string")
                .build();
    }
    public static UserDto getAdminDto() {
        return UserDto.builder()
                .id(adminRandomUUID)
                .name("user_name")
                .surname("user_surname")
                .email("mail@mail.ru")
                .roles(List.of(Role.ROLE_USER))
                .avatar("base64string")
                .build();
    }
    public static JwtUser getJwtUser() {
        return JwtUser.builder()
                .id(userRandomUUID)
                .email("mail@mail.ru")
                .password("12")
                .name("user_name")
                .surname("user_surname")
                .authorities(Collections.singletonList(new SimpleGrantedAuthority(Role.ROLE_USER.name())))
                .build();
    }
}
